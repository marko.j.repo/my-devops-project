FROM node:latest

WORKDIR /app

COPY package*.json ./

RUN npm ci

COPY . .

EXPOSE 3000

# specify the command which runs the application

CMD ["npm", "start"]
